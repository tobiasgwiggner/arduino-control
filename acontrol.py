#!/bin/python3

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMessageBox
import sys
import window
import serial

ser = serial.Serial()
ser.baudrate = 9600

def serial_ports():
    """
    Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    
    (https://stackoverflow.com/questions/12090503/listing-available-com-ports-with-python#14224477)
    """

    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

class AControl(QtWidgets.QMainWindow, window.Ui_ArduinoControl):
    def __init__(self, parent=None):
        super(AControl, self).__init__(parent)
        self.setupUi(self)

        # Add TTYs to combo box
        targets = serial_ports()
        if targets != []:
            self.connect.setEnabled(True)
            for target in targets:
                self.targets.addItem(target)
        else:
            self.connect.setEnabled(False)

        # Connect signals
        self.connect.clicked.connect(self.connectArduino)
        self.routine_a.clicked.connect(self.routine_a_f)
        self.routine_b.clicked.connect(self.routine_b_f)
        self.routine_c.clicked.connect(self.routine_c_f)
        self.routine_d.clicked.connect(self.routine_d_f)

    def connectArduino(self):
        try:
            # Connect
            ser.port = self.targets.currentText()
            ser.open()
            self.connect.setText("Disconnect")
            self.connect.clicked.disconnect()
            self.connect.clicked.connect(self.disconnectArduino)

            # Enable routine buttons
            self.routine_a.setEnabled(True)
            self.routine_b.setEnabled(True)
            self.routine_c.setEnabled(True)
            self.routine_d.setEnabled(True)

        except Exception as e:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error")
            msg.setInformativeText(str(e))
            msg.setWindowTitle("Error")
            msg.exec_()

    def disconnectArduino(self):
        # Disconnect
        ser.close()
        self.connect.setText("Connect")
        self.connect.clicked.disconnect()
        self.connect.clicked.connect(self.connectArduino)

        # Disable routine buttons
        self.routine_a.setEnabled(False)
        self.routine_b.setEnabled(False)
        self.routine_c.setEnabled(False)
        self.routine_d.setEnabled(False)

    def routine_func(self, cmd):
        try:
            ser.write(cmd)
        except Exception as e:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error")
            msg.setInformativeText(str(e))
            msg.setWindowTitle("Error")
            msg.exec_()

    def routine_a_f(self):
        self.routine_func(b"a")
    def routine_b_f(self):
        self.routine_func(b"b")
    def routine_c_f(self):
        self.routine_func(b"c")
    def routine_d_f(self):
        self.routine_func(b"d")

def main():
    app = QApplication(sys.argv)
    form = AControl()
    form.show()
    app.exec_()
    ser.close()

if __name__ == '__main__':
    main()
