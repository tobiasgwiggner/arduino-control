# Arduino Control

Start specific functions on Arduino by sending predefined commands over serial

## Development

Install dependencies: 

```sh
pip3 install --user -r requirements.txt
```

Generate window module from Qt UI file:

```sh
pyuic5 window.ui -o window.py
```

Run:

```sh
python acontrol.py
```

Bundle:

```bat
# Add DLLs to Path on Windows
set PATH=%PATH%;C:\Windows\System32\downlevel;
# Build EXE
pyinstaller -F acontrol.spec
```
